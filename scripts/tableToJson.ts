import * as fs from 'fs';
import * as zlib from 'zlib';
import * as readline from 'readline';

function readTsvFile(path: string) {
    let stream: NodeJS.ReadableStream = fs.createReadStream(path);
    if (/\.gz$/i.test(path)) {
        stream = stream.pipe(zlib.createGunzip())
    }

    return readline.createInterface(
        {
            input: stream,
            crlfDelay: Infinity
        }
    )
}

async function main() {
    const lineReader = readTsvFile('./db/tableInfo.tsv.gz');
    const data = [];

    for await (const line of lineReader) {
        if (line.startsWith('#')) {
            continue;
        }
        const [protein, gene, product, note, counter] = line.split('\t');
        const processedNote = (note === '<note>') ? '' : note;

        data.push({
            protein,
            gene,
            product,
            note: processedNote
        });
    }

    fs.writeFileSync('./db/output.json', JSON.stringify(data, null, 2), 'utf8');
}

main().catch(err => {
    console.error(err);
    process.exit(1);
});
