import Database from 'better-sqlite3';

// define records item type
export interface SyndiveRecord {
    id: number;
    protein: string;
    gene: string;
    product: string;
    note: string;
    rank: number;
  }


// initialize database
const db = new Database('./db/syndive.sqlite');
export default db;


// create the database schema
export function createSyndiveDatabaseSchema() {
    db.exec(`
        DROP TABLE IF EXISTS records;
        CREATE TABLE records (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            protein TEXT,
            gene TEXT,
            product TEXT,
            note TEXT
        );
    `);

    db.exec(`
        DROP TABLE IF EXISTS records_fts;
        CREATE VIRTUAL TABLE records_fts
        USING fts5(protein, gene, product, note, content='records', content_rowid='id', tokenize='trigram');
    `);

    // Create triggers to keep the FTS table in sync
    db.exec(`
        CREATE TRIGGER records_ai AFTER INSERT ON records
        BEGIN
            INSERT INTO records_fts (rowid, protein, gene, product, note)
            VALUES (new.id, new.protein, new.gene, new.product, new.note);
        END;
    `);

    db.exec(`
        CREATE TRIGGER records_ad AFTER DELETE ON records
        BEGIN
            INSERT INTO records_fts (records_fts, rowid, protein, gene, product, note)
            VALUES ('delete', old.id, old.protein, old.gene, old.product, old.note);
        END;
    `);

    db.exec(`
        CREATE TRIGGER records_au AFTER UPDATE ON records
        BEGIN
            INSERT INTO records_fts (records_fts, rowid, protein, gene, product, note)
            VALUES ('delete', old.id, old.protein, old.gene, old.product, old.note);
            INSERT INTO records_fts (rowid, protein, gene, product, note)
            VALUES (new.id, new.protein, new.gene, new.product, new.note);
        END;
    `);

}


// get the row count of a table
export function countTableRows(table: string): number {
    const command = `SELECT COUNT(*) AS count FROM ${table}`;
    const row = db.prepare(command).get() as { count: number };
    return row.count;
}


// prepared statement for inserting rows into the records table
export async function insertRowToRecords(protein: string, gene: string, 
                                         product: string, note: string) {
    const command = `
        INSERT INTO records (protein, gene, product, note) VALUES (?, ?, ?, ?)
    `;
    db.prepare(command).run(protein, gene, product, note);
} 


// autocomplete on records table
export async function queryRecord(query: string, offset: number = 0, limit: number = 10): Promise<SyndiveRecord[]> {
    const command = `
      SELECT records.id, records.protein, records.gene, records.product, records.note, records_fts.rank
      FROM records
      JOIN records_fts ON records.rowid = records_fts.rowid
      WHERE records_fts MATCH ?
      ORDER BY records_fts.rank
      LIMIT ? OFFSET ?
    `;
    return db.prepare(command).all(query, limit, offset) as SyndiveRecord[];
  }