import db, { queryRecord, SyndiveRecord } from '@/scripts/dbtools';

async function main() {
    
    // Test the search function
    const testQuery = 'Camk'; // Replace with a meaningful test query
    const searchResults = await queryRecord(testQuery);

    console.log(`Search results for query "${testQuery}":`);
    searchResults.forEach(result => {
        console.log(result);
    });

    db.close();
}


main().catch(err => {
    console.error(err);
    process.exit(1);
});