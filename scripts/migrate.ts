import db, { 
    createSyndiveDatabaseSchema,
    countTableRows,
    insertRowToRecords 
} from '@/scripts/dbtools';

import * as fs from 'fs';
import * as zlib from 'zlib';
import * as readline from 'readline';


function readTsvFile(path: string) {
    let stream: NodeJS.ReadableStream = fs.createReadStream(path);
    if (/\.gz$/i.test(path)) {
        stream = stream.pipe(zlib.createGunzip())
    }

    return readline.createInterface(
        {
            input: stream,
            crlfDelay: Infinity
        }
    )
}


async function main() {
    const lineReader = readTsvFile('./db/tableInfo.tsv.gz');

    createSyndiveDatabaseSchema();

    for await(const line of lineReader) {
        if (line.startsWith('#')) {
            continue;
        }
        const [protein, gene, product, note, counter] = line.split('\t');
        const processedNote = (note == '<note>') ? '' : note;
        insertRowToRecords(protein, gene, product, processedNote);
    }

    console.log('Data insertion complete');
    console.log(`Number of rows in table records: ${countTableRows('records')}`);
    console.log(`Number of rows in table records_fts: ${countTableRows('records_fts')}`);
    db.close();
}


main().catch(err => {
    console.error(err);
    process.exit(1);
})
