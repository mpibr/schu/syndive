'use client'
import React, { useState, useEffect, useCallback } from "react";
import { Modal, ModalContent, ModalHeader, ModalBody } from "@nextui-org/modal";
import { Autocomplete, AutocompleteItem } from "@nextui-org/autocomplete";
import { SyndiveRecord } from '@/scripts/dbtools';
import debounce from 'lodash/debounce';

interface SearcherProps {
  isOpen: boolean;
  onClose: () => void;
}

export const Searcher: React.FC<SearcherProps> = ({ isOpen, onClose }) => {
  const [items, setItems] = useState<SyndiveRecord[]>([]);
  const [query, setQuery] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchDefaultItems = async () => {
      setLoading(true);
      try {
        const response = await fetch("/api/search?query=Camk2a");
        if (!response.ok) {
          throw new Error('Failed to fetch default suggestions');
        }
        const data: SyndiveRecord[] = await response.json();
        setItems(data);
      } catch (error) {
        setError('Failed to fetch default suggestions');
      } finally {
        setLoading(false);
      }
    };
    fetchDefaultItems();
  }, []);

  const handleSearch = async (query: string) => {
    setLoading(true);
    try {
      const response = await fetch(`/api/search?query=${encodeURIComponent(query)}`);
      if (!response.ok) {
        throw new Error('Failed to fetch search results');
      }
      const data: SyndiveRecord[] = await response.json();
      setItems(data);
    } catch (error) {
      setError('Failed to fetch search results');
    } finally {
      setLoading(false);
    }
  };

  const debouncedHandleSearch = useCallback(debounce((query: string) => {
    handleSearch(query);
  }, 500), []);

  const onInputChange = (value: string) => {
    setQuery(value);
    if (value.length < 3) {
      setError('Try to add more characters');
      setItems([]);
    } else {
      debouncedHandleSearch(value);
    }
  };


  return (
    <Modal isOpen={isOpen} onOpenChange={onClose} placement="top-center">
      <ModalContent>
        <>
          <ModalHeader className="flex flex-col gap-1">Search</ModalHeader>
          <ModalBody>
            {error && <div className="text-red-500">{error}</div>}
            {loading ? (
              <div>Loading...</div>
            ) : (
              <Autocomplete
                label="Gene, protein or product name"
                items={items}
                placeholder="Search a candidate"
                className="max-w-xs"
                onInputChange={onInputChange}
              >
                {(item) => (
                  <AutocompleteItem key={item.id} value={item.protein}>
                    {item.protein} - {item.gene} - {item.product}
                  </AutocompleteItem>
                )}
              </Autocomplete>
            )}
          </ModalBody>
        </>
      </ModalContent>
    </Modal>
  );
};
