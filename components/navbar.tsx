"use client";

import React, { useEffect, useState } from "react";
import {
  Navbar as NextUINavbar,
  NavbarContent,
  NavbarMenu,
  NavbarMenuToggle,
  NavbarBrand,
  NavbarItem,
  NavbarMenuItem,
} from "@nextui-org/navbar";
import { dataFocusVisibleClasses } from "@nextui-org/theme";
import { Kbd } from "@nextui-org/kbd";
import { Link } from "@nextui-org/link";
import { Button } from "@nextui-org/button";
import { link as linkStyles } from "@nextui-org/theme";
import NextLink from "next/link";
import { siteConfig } from "@/config/site";

import { clsx } from "@nextui-org/shared-utils";

import { ThemeSwitch } from "@/components/theme-switch";
import { SearchIcon, LogoSyndive } from "@/components/icons";
import { usePress } from "@react-aria/interactions";
import { useFocusRing } from "@react-aria/focus";
import { isAppleDevice } from "@react-aria/utils";
import { Searcher } from "./searcher";

export const Navbar = () => {
  const [isMenuOpen, setIsMenuOpen] = useState<boolean | undefined>(false);
  const [isSearcherOpen, setIsSearcherOpen] = useState(false);
  const [commandKey, setCommandKey] = useState<"ctrl" | "command">("command");

  useEffect(() => {
    setCommandKey(isAppleDevice() ? "command" : "ctrl");
  }, []);

  const handleOpenSearcher = () => {
    setIsSearcherOpen(true);
  };

  const handleCloseSearcher = () => {
    setIsSearcherOpen(false);
  };

  const { pressProps } = usePress({
    onPress: handleOpenSearcher,
  });
  const { focusProps, isFocusVisible } = useFocusRing();

  const searchButton = (
    <Button
      aria-label="Quick search"
      className="text-sm font-normal text-default-500 bg-default-400/20 dark:bg-default-500/20"
      endContent={
        <Kbd className="hidden py-0.5 px-2 lg:inline-block" keys={commandKey}>
          K
        </Kbd>
      }
      startContent={
        <SearchIcon
          className="text-base text-default-400 pointer-events-none flex-shrink-0"
          size={18}
        />
      }
      onPress={handleOpenSearcher}
    >
      Quick Search...
    </Button>
  );

  const searchIconButton = (
    <button
      aria-label="Quick search"
      className={clsx(
        "transition-opacity p-1 hover:opacity-80 rounded-full cursor-pointer outline-none",
        ...dataFocusVisibleClasses
      )}
      data-focus-visible={isFocusVisible}
      {...focusProps}
      {...pressProps}
    >
      <SearchIcon className="text-default-600 dark:text-default-500" size={20} />
    </button>
  );

  return (
    <>
      <NextUINavbar isBordered maxWidth="xl" position="sticky">
        <NavbarContent className="basis-1/5 sm:basis-full" justify="start">
          <NavbarBrand as="li" className="gap-3 max-w-fit">
            <NextLink className="flex justify-start items-center gap-1" href="/">
              <LogoSyndive />
              <p className="font-bold text-inherit">syndive</p>
            </NextLink>
          </NavbarBrand>
          <ul className="hidden lg:flex gap-4 justify-start ml-2">
            {siteConfig.navMenuItems.map((item) => (
              <NavbarItem key={item.href}>
                <NextLink
                  className={clsx(
                    linkStyles({ color: "foreground" }),
                    "data-[active=true]:text-primary data-[active=true]:font-medium"
                  )}
                  color="foreground"
                  href={item.href}
                >
                  {item.label}
                </NextLink>
              </NavbarItem>
            ))}
          </ul>
        </NavbarContent>

        <NavbarContent className="hidden sm:flex basis-1/5 sm:basis-full" justify="end">
          <NavbarItem className="hidden lg:flex">{searchButton}</NavbarItem>
          <NavbarItem className="hidden sm:flex gap-2">
            <ThemeSwitch />
          </NavbarItem>
        </NavbarContent>

        <NavbarContent className="sm:hidden basis-1 pl-4" justify="end">
          {searchIconButton}
          <ThemeSwitch />
          <NavbarMenuToggle />
        </NavbarContent>

        <NavbarMenu>
          {searchButton}
          <div className="mx-4 mt-2 flex flex-col gap-2">
            {siteConfig.navItems.map((item, index) => (
              <NavbarMenuItem key={`${item}-${index}`}>
                <NextLink
                  color={
                    index === 1
                      ? "secondary"
                      : index === siteConfig.navItems.length - 1
                      ? "primary"
                      : "foreground"
                  }
                  href={item.href}
                >
                  {item.label}
                </NextLink>
              </NavbarMenuItem>
            ))}
          </div>
        </NavbarMenu>
      </NextUINavbar>
      <Searcher isOpen={isSearcherOpen} onClose={handleCloseSearcher} />
    </>
  );
};
