import { NextRequest, NextResponse } from 'next/server';
import { queryRecord, SyndiveRecord } from '@/scripts/dbtools';

export async function GET(req: NextRequest) {
  const { searchParams } = new URL(req.url);
  const query = searchParams.get('query');
  const limit = parseInt(searchParams.get('limit') || '10', 10);

  if (typeof query !== 'string' || isNaN(limit)) {
    return NextResponse.json({ error: 'Invalid query' }, { status: 400 });
  }

  try {
    const results: SyndiveRecord[] = await queryRecord(query, limit);
    return NextResponse.json(results, { status: 200 });
  } catch (error) {
    console.error('Error fetching search results:', error);
    return NextResponse.json({ error: 'Internal Server Error' }, { status: 500 });
  }
}
