import { Link } from "@nextui-org/link";
import NextLink from "next/link";
import { Snippet } from "@nextui-org/snippet";
import { Code } from "@nextui-org/code"
import { button as buttonStyles } from "@nextui-org/theme";
import { siteConfig } from "@/config/site";
import { title, subtitle } from "@/components/primitives";
import { MpiBrIcon } from "@/components/icons";

export default function Home() {
	return (
		<section className="flex flex-col items-center justify-center gap-4 py-8 md:py-10">
			<div className="inline-block max-w-lg text-center justify-center">
				<h1 className={title({ color: "grey" })}>Synaptic&nbsp;</h1>
				<h1 className={title({ color: "grey" })}>Diversity&nbsp;</h1>
				<h1 className={title({ color: "grey" })}>Hub</h1>
				<br />
				<h2 className={subtitle({ class: "mt-4" })}>
					A collection of transcripts and proteins localised to neurites and
					synapses of excitatory and inhibitory neurons. The data is published
					by the Schuman Lab at MPI for Brain Research.
				</h2>
			</div>

			<div className="flex gap-3">

				<NextLink
					href="/publications"
					className={buttonStyles({ color: "secondary", radius: "full" })}
				>
					Publications
				</NextLink>
				<Link
					isExternal
					className={buttonStyles({ variant: "bordered", radius: "full" })}
					href={siteConfig.links.research}
				>
					<MpiBrIcon size={20} />
					Research
				</Link>
			</div>

			<div className="mt-8">
				<Snippet hideSymbol hideCopyButton variant="flat">
					<span>
						Get started by typing&nbsp;your favorite&nbsp;
						<Code color="secondary">
							candidate
						</Code>
					</span>
				</Snippet>
			</div>
		</section>
	);
}
