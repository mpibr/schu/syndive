import { title } from "@/components/primitives";

export default function ExportsPage() {
	return (
		<div>
			<h1 className={title()}>Exports</h1>
		</div>
	);
}
