import { title } from "@/components/primitives";

export default function PublicationsPage() {
	return (
		<div>
			<h1 className={title()}>Publications</h1>
		</div>
	);
}
