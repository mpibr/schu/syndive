export type SiteConfig = typeof siteConfig;

const navItems = [
	{ label: "Home", href: "/" },
	{ label: "Dashboard", href: "/dashboard" },
	{ label: "Exports", href: "/exports" },
	{ label: "About", href: "/about" },
	{ label: "Publications", href: "/publications"}
  ];

export const siteConfig = {
	name: "syndive",
	description: "Make diverse synapses regardless of your prior experience.",
	navMenuItems: navItems.slice(0, 4),
	navItems: navItems,
	links: {
		xtwitter: "https://twitter.com/MpiBrain",
		research: "https://brain.mpg.de/schuman",
		mpibr: "https://brain.mpg.de/home",
		gitlab: "https://gitlab.mpcdf.mpg.de/mpibr",
		code: "https://gitlab.mpcdf.mpg.de/mpibr/schu/syndive",
		publications: navItems.find(item => item.label === "Publications")?.href
	},
};
